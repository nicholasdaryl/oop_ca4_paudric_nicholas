package client;

import core.MovieDTO;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import org.json.simple.JSONObject;
import core.ServiceDetails;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Nick D and Paudric S
 */
public class Client {

    // options
    private static final int LOGIN = 1;
    private static final int REGISTER = 2;
    private static final int MOVIES = 3;
    private static final int EXIT = 4;
    private static final int WATCH_MOVIES = 1;
    private static final int ADD_MOVIES = 2;
    private static final int DELETE_MOVIES = 3;
    private static final int UPDATE_MOVIES = 4;
    private static final int BACK = 5;
    private static final int BY_NAME = 1;
    private static final int BY_GENRE = 2;
    private static final int BY_YEAR = 3;

    // connections stuff
    private static Socket connectionSocket = null;
    private static PrintWriter outToServer = null;
    private static BufferedReader inFromServer = null;
    private static String request = "";
    private static String response = "";

    // cache
    // map of which key is the request and value is the response text
    private static Map<String, String> cache = new HashMap<>();

    public static void main(String[] args) {

        //Utils.validateName(xxxxxxxxxxxxx);
        // booleans for validations or looping
        boolean valid = false, moviesValid = false, watchMoviesValid = false;

        // user input stuff
        Scanner userInput = new Scanner(System.in);
        int choice = 0, moviesChoice = 0, watchMovieChoice = 0;
        String stringInput = "";
        boolean loggedIn = false;
        boolean registered = false;

        // stored data
        String storedName = "";
        String storedId = "";
        String storedAge = "";

        // Results
        MovieDTO[] movieResults;

        while (!valid) {
            try {
                moviesValid = false;

                System.out.println("\n\n************************************");
                System.out.println("*** Welcome to the movie website ***");
                System.out.println("************************************");
                System.out.println("1. Login here if Registered."
                        + "\n2. Register."
                        + "\n3. Movies(Must be logged in)."
                        + "\n4. Exit Application.");

                System.out.print("\nEnter Option:");
                userInput = new Scanner(System.in);
                choice = userInput.nextInt();
                userInput.nextLine();

                switch (choice) {
                    case LOGIN:

                        if (storedId.equals("")) {
                            System.out.println("\n\n************************************");
                            System.out.println("***             Login            ***");
                            System.out.println("************************************");
                            System.out.print("Please enter your username: ");
                            Scanner scannedName = new Scanner(System.in);
                            String username = scannedName.nextLine();
                            System.out.print("Please enter your password: ");
                            Scanner scannedPassword = new Scanner(System.in);
                            String password = scannedPassword.nextLine();

                            request = ServiceDetails.LOGIN_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                    + username + ServiceDetails.BREAKINGCHARACTERS + password;
                            // send the request to the server
                            try {
                                // send through output stream
                                sendRequest();
                                // get response back from the server
                                receiveResponse();
                                // close the connection 
                                closeConnection();

                                // deal with response data
                                if (response.equals(ServiceDetails.USER_NOT_FOUND_RESPONSE)) {
                                    System.out.println("The username or password you entered is not found!");
                                    break;
                                } else {
                                    String[] splitResponse = response.split(ServiceDetails.BREAKINGCHARACTERS);
                                    System.out.println("You have logged in successfully as " + splitResponse[1]);
                                    storedId = splitResponse[0];
                                    storedName = splitResponse[1];
                                    storedAge = splitResponse[2];
                                    loggedIn = true;
                                    break;
                                }

                                // display movies
                                //System.out.println(printArray(results));
                            } catch (Exception e) {
                                if (e.getMessage() != null) {
                                    checkAndDisplayErrorMessage(e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            System.out.println("\n\n************************************");
                            System.out.println("***             Login            ***");
                            System.out.println("************************************");
                            System.out.println("You must be registered!");
                        }

                        break;

                    case REGISTER:

                        if (!registered && !loggedIn) {

                            System.out.println("\n\n************************************");
                            System.out.println("***            Register          ***");
                            System.out.println("************************************");

                            while (!registered && !loggedIn) {
                                System.out.print("Please enter your username: ");
                                Scanner scanner = new Scanner(System.in);
                                String username = scanner.nextLine();
                                boolean matched = Pattern.matches("^[A-Za-z0-9_-]{4,20}$", username);
                                while (!matched) {
                                    System.out.println("Username must be between 4 and 20 characters \n"
                                            + "and can only contain letters, numbers, underscores and hyphens: ");
                                    scanner = new Scanner(System.in);
                                    username = scanner.nextLine();

                                    matched = Pattern.matches("^[A-Za-z0-9_-]{4,20}$", username);

                                }

                                System.out.print("Please enter your password: ");
                                scanner = new Scanner(System.in);
                                String password = scanner.nextLine();

                                if (!isValidPassword(password)) {
                                    matched = false;
                                } else {
                                    matched = true;
                                }

                                while (!matched) {
                                    System.out.print("Password must be between 8 and 16 characters and must contain \n"
                                            + "at least one Uppercase, one lowercase and one number: ");
                                    scanner = new Scanner(System.in);
                                    password = scanner.nextLine();

                                    if (!isValidPassword(password)) {
                                        matched = false;
                                    } else {
                                        matched = true;
                                    }

                                }

                                System.out.print("Please enter your age: ");
                                scanner = new Scanner(System.in);
                                int age = scanner.nextInt();
                                scanner.nextLine();

                                while (age < 6) {
                                    System.out.print("You must 6 or older to use this website: ");
                                    scanner = new Scanner(System.in);
                                    age = scanner.nextInt();

                                }

                                request = ServiceDetails.REGISTER_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                        + username + ServiceDetails.BREAKINGCHARACTERS + password
                                        + ServiceDetails.BREAKINGCHARACTERS + age;
                                // send the request to the server
                                try {
                                    // send through output stream
                                    sendRequest();
                                    // get response back from the server
                                    receiveResponse();
                                    // close the connection 
                                    closeConnection();

                                    // deal with response data
                                    if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                        System.out.println("An error has occurred, please try again!");
                                        break;
                                    } else {
                                        String[] splitResponse = response.split(ServiceDetails.BREAKINGCHARACTERS);
                                        System.out.println("You have Registered successfully!");
                                        registered = true;
                                        break;
                                    }

                                    // display movies
                                    //System.out.println(printArray(results));
                                } catch (Exception e) {
                                    if (e.getMessage() != null) {
                                        checkAndDisplayErrorMessage(e.getMessage());
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {
                            System.out.println("\n\n************************************");
                            System.out.println("***            Register          ***");
                            System.out.println("************************************");
                            System.out.println("You are already Registered!");
                            break;
                        }

                        break;

                    case MOVIES:
                        while (!moviesValid) {
                            watchMoviesValid = false;

                            if (!loggedIn) {
                                System.out.println("***You must be logged in to modify Movies ***");
                                break;
                            } else {
                                System.out.println("\n\n************************************");
                                System.out.println("***           Movies             ***");
                                System.out.println("************************************");
                                System.out.println("1. Watch Movies."
                                        + "\n2. Add Movies."
                                        + "\n3. Delete Movies."
                                        + "\n4. Update Movies."
                                        + "\n5. Go Back.");

                                System.out.print("\nEnter Option:");
                                moviesChoice = userInput.nextInt();

                                switch (moviesChoice) {
                                    case WATCH_MOVIES:
                                        MovieDTO[] results = null;
                                        while (!watchMoviesValid) {
                                            request = ServiceDetails.RECOMMEND_COMMAND + ServiceDetails.BREAKINGCHARACTERS + storedId;

                                            // send the request to the server
                                            try {
                                                // send through output stream
                                                sendRequest();
                                                // get response back from the server
                                                receiveResponse();
                                                // close the connection 
                                                closeConnection();

                                                // deal with response data
                                                JSONObject[] responseJSON = ServiceDetails.stringToJSON(response);
                                                results = MovieDTO.JSONToMovieDTO(responseJSON);

                                            } catch (Exception e) {
                                                if (e.getMessage() != null) {
                                                    checkAndDisplayErrorMessage(e.getMessage());
                                                    e.printStackTrace();
                                                }
                                            }
                                            System.out.println("\n\n************************************");
                                            System.out.println("***           Movies             ***");
                                            System.out.println("************************************");
                                            System.out.println("1. " + results[0].getTitle() + ", "
                                                    + results[0].getGenre() + ", "
                                                    + results[0].getDirector() + ", "
                                                    + results[0].getYear() + ", "
                                                    + results[0].getRuntime() + ", "
                                                    + "\n2. " + results[1].getTitle() + ", "
                                                    + results[1].getGenre() + ", "
                                                    + results[1].getDirector() + ", "
                                                    + results[1].getYear() + ", "
                                                    + results[1].getRuntime() + ", "
                                                    + "\n3. " + results[2].getTitle() + ", "
                                                    + results[2].getGenre() + ", "
                                                    + results[2].getDirector() + ", "
                                                    + results[2].getYear() + ", "
                                                    + results[2].getRuntime() + ", "
                                                    + "\n4. " + results[3].getTitle() + ", "
                                                    + results[3].getGenre() + ", "
                                                    + results[3].getDirector() + ", "
                                                    + results[3].getYear() + ", "
                                                    + results[3].getRuntime() + ", "
                                                    + "\n5. " + results[4].getTitle() + ", "
                                                    + results[4].getGenre() + ", "
                                                    + results[4].getDirector() + ", "
                                                    + results[4].getYear() + ", "
                                                    + results[4].getRuntime() + ", "
                                                    + "\n********************************************."
                                                    + "\n\n6. Search movies."
                                                    + "\n7. Show all movies."
                                                    + "\n8. Go back.");

                                            System.out.print("\nEnter Option:");
                                            watchMovieChoice = userInput.nextInt();

                                            switch (watchMovieChoice) {
                                                case 1:

                                                    request = ServiceDetails.WATCH_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                                            + storedId + ServiceDetails.BREAKINGCHARACTERS
                                                            + results[0].getId();

                                                    // send the request to the server
                                                    try {
                                                        // send through output stream
                                                        sendRequest();
                                                        // get response back from the server
                                                        receiveResponse();
                                                        // close the connection 
                                                        closeConnection();

                                                        // deal with response data
                                                        if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                                            System.out.println("An error has occurred, please try again!");
                                                            break;
                                                        } else {
                                                            System.out.println("You have just watched " + results[0].getTitle());
                                                            break;
                                                        }

                                                    } catch (Exception e) {
                                                        if (e.getMessage() != null) {
                                                            checkAndDisplayErrorMessage(e.getMessage());
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                    break;
                                                case 2:
                                                    request = ServiceDetails.WATCH_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                                            + storedId + ServiceDetails.BREAKINGCHARACTERS
                                                            + results[1].getId();

                                                    // send the request to the server
                                                    try {
                                                        // send through output stream
                                                        sendRequest();
                                                        // get response back from the server
                                                        receiveResponse();
                                                        // close the connection 
                                                        closeConnection();

                                                        // deal with response data
                                                        if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                                            System.out.println("An error has occurred, please try again!");
                                                            break;
                                                        } else {
                                                            System.out.println("You have just watched " + results[1].getTitle());
                                                            break;
                                                        }

                                                    } catch (Exception e) {
                                                        if (e.getMessage() != null) {
                                                            checkAndDisplayErrorMessage(e.getMessage());
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                    break;

                                                case 3:
                                                    request = ServiceDetails.WATCH_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                                            + storedId + ServiceDetails.BREAKINGCHARACTERS
                                                            + results[2].getId();

                                                    // send the request to the server
                                                    try {
                                                        // send through output stream
                                                        sendRequest();
                                                        // get response back from the server
                                                        receiveResponse();
                                                        // close the connection 
                                                        closeConnection();

                                                        // deal with response data
                                                        if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                                            System.out.println("An error has occurred, please try again!");
                                                            break;
                                                        } else {
                                                            System.out.println("You have just watched " + results[2].getTitle());
                                                            break;
                                                        }

                                                    } catch (Exception e) {
                                                        if (e.getMessage() != null) {
                                                            checkAndDisplayErrorMessage(e.getMessage());
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                    break;

                                                case 4:
                                                    request = ServiceDetails.WATCH_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                                            + storedId + ServiceDetails.BREAKINGCHARACTERS
                                                            + results[3].getId();

                                                    // send the request to the server
                                                    try {
                                                        // send through output stream
                                                        sendRequest();
                                                        // get response back from the server
                                                        receiveResponse();
                                                        // close the connection 
                                                        closeConnection();

                                                        // deal with response data
                                                        if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                                            System.out.println("An error has occurred, please try again!");
                                                            break;
                                                        } else {
                                                            System.out.println("You have just watched " + results[3].getTitle());
                                                            break;
                                                        }

                                                    } catch (Exception e) {
                                                        if (e.getMessage() != null) {
                                                            checkAndDisplayErrorMessage(e.getMessage());
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                    break;

                                                case 5:
                                                    request = ServiceDetails.WATCH_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                                            + storedId + ServiceDetails.BREAKINGCHARACTERS
                                                            + results[4].getId();

                                                    // send the request to the server
                                                    try {
                                                        // send through output stream
                                                        sendRequest();
                                                        // get response back from the server
                                                        receiveResponse();
                                                        // close the connection 
                                                        closeConnection();

                                                        // deal with response data
                                                        if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                                            System.out.println("An error has occurred, please try again!");
                                                            break;
                                                        } else {
                                                            System.out.println("You have just watched " + results[4].getTitle());
                                                            break;
                                                        }

                                                    } catch (Exception e) {
                                                        if (e.getMessage() != null) {
                                                            checkAndDisplayErrorMessage(e.getMessage());
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                    break;

                                                case 6:

                                                    // search
                                                    movieResults = searchMovies();
                                                    if (movieResults == null) {
                                                        break;
                                                    }
                                                    displayMovieResults(movieResults);

                                                    // choose which to watch
                                                    while (!valid) {
                                                        System.out.print("\nChoose a movie from the list to update (list number): ");
                                                        userInput = new Scanner(System.in);
                                                        moviesChoice = userInput.nextInt();

                                                        if (moviesChoice < 0 || moviesChoice > movieResults.length) {
                                                            valid = false;
                                                            System.out.println("\n please enter a valid choice.");
                                                        } else {

                                                            request = ServiceDetails.WATCH_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                                                    + storedId + ServiceDetails.BREAKINGCHARACTERS
                                                                    + movieResults[moviesChoice - 1].getId();

                                                            // send the request to the server
                                                            try {
                                                                // send through output stream
                                                                sendRequest();
                                                                // get response back from the server
                                                                receiveResponse();
                                                                // close the connection 
                                                                closeConnection();

                                                                // deal with response data
                                                                if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                                                    System.out.println("An error has occurred, please try again!");
                                                                    break;
                                                                } else {
                                                                    System.out.println("You have just watched " + movieResults[moviesChoice - 1].getTitle());
                                                                    break;
                                                                }

                                                            } catch (Exception e) {
                                                                if (e.getMessage() != null) {
                                                                    checkAndDisplayErrorMessage(e.getMessage());
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }
                                                    }
                                                    valid = false;

                                                    break;

                                                case 7:
                                                    System.out.println("Here are the list of the first 25 movies from the database.");
                                                    // assign the request
                                                    request = ServiceDetails.GET_MOVIES_COMMAND;

                                                    // send the request to the server
                                                    try {
                                                        // send through output stream
                                                        sendRequest();
                                                        // get response back from the server
                                                        receiveResponse();
                                                        // close the connection 
                                                        closeConnection();

                                                        // deal with response data
                                                        JSONObject[] responseJSON = ServiceDetails.stringToJSON(response);
                                                        results = MovieDTO.JSONToMovieDTO(responseJSON);

                                                        // display movies
                                                        System.out.println(printArray(results));

                                                    } catch (Exception e) {
                                                        if (e.getMessage() != null) {
                                                            checkAndDisplayErrorMessage(e.getMessage());
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                    break;

                                                case 8:
                                                    watchMoviesValid = true;
                                                    break;

                                                default:
                                                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                                                    System.out.println("!!!!! Please enter a valid option !!!!!");
                                                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                                                    break;
                                            }
                                        }
                                        break;

                                    case ADD_MOVIES:
                                        if (loggedIn) {

                                            Scanner scanner = new Scanner(System.in);
                                            System.out.print("Please enter the title: ");
                                            String title = scanner.nextLine();
                                            MovieDTO movieDto = new MovieDTO(title);
                                            System.out.print("Please enter the genre: ");
                                            String genre = scanner.nextLine();
                                            movieDto.setGenre(genre);
                                            System.out.print("Please enter the director: ");
                                            String director = scanner.nextLine();
                                            movieDto.setDirector(director);
                                            System.out.print("Please enter the runtime: ");
                                            String runtime = scanner.nextLine();
                                            movieDto.setRuntime(runtime);
                                            System.out.print("Please enter the plot: ");
                                            String plot = scanner.nextLine();
                                            movieDto.setPlot(plot);
                                            System.out.print("Please enter the location: ");
                                            String location = scanner.nextLine();
                                            movieDto.setLocation(location);
                                            System.out.print("Please enter the poster: ");
                                            String poster = scanner.nextLine();
                                            movieDto.setPoster(poster);
                                            System.out.print("Please enter the rating: ");
                                            String rating = scanner.nextLine();
                                            movieDto.setRating(rating);
                                            System.out.print("Please enter the format: ");
                                            String format = scanner.nextLine();
                                            movieDto.setFormat(format);
                                            System.out.print("Please enter the year: ");
                                            String year = scanner.nextLine();
                                            movieDto.setYear(year);
                                            System.out.print("Please enter the starring: ");
                                            String starring = scanner.nextLine();
                                            movieDto.setStarring(starring);
                                            System.out.print("Please enter the copies: ");
                                            int copies = scanner.nextInt();
                                            movieDto.setCopies(copies);
                                            System.out.print("Please enter the barcode: ");
                                            String barcode = scanner.nextLine();
                                            movieDto.setBarcode(barcode);
                                            System.out.print("Please enter the userRating: ");
                                            String userRating = scanner.nextLine();
                                            movieDto.setUser_rating(userRating);

                                            request = ServiceDetails.ADD_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                                    + movieDto.toJSONString();

                                            // send the request to the server
                                            try {
                                                // send through output stream
                                                sendRequest();
                                                // get response back from the server
                                                receiveResponse();
                                                // close the connection 
                                                closeConnection();

                                                // deal with response data
                                                if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                                    System.out.println("An error has occurred, please try again!");
                                                    break;
                                                } else {
                                                    System.out.println("Movie added successfully!");
                                                    break;
                                                }

                                            } catch (Exception e) {
                                                if (e.getMessage() != null) {
                                                    checkAndDisplayErrorMessage(e.getMessage());
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                        break;

                                    case DELETE_MOVIES:
                                        if (loggedIn) {
                                            movieResults = searchMovies();

                                            if (movieResults == null) {
                                                break;
                                            }

                                            displayMovieResults(movieResults);

                                            // choose movie to delete
                                            System.out.print("Enter movie to delete: ");
                                            Scanner scanner = new Scanner(System.in);
                                            moviesChoice = scanner.nextInt();

                                            request = ServiceDetails.DELETE_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                                    + movieResults[moviesChoice - 1].getId();

                                            // send the request to the server
                                            try {
                                                // send through output stream
                                                sendRequest();
                                                // get response back from the server
                                                receiveResponse();
                                                // close the connection 
                                                closeConnection();

                                                // deal with response data
                                                if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                                    System.out.println("An error has occurred, please try again!");
                                                    break;
                                                } else {
                                                    System.out.println("Movie deleted successfully!");
                                                    break;
                                                }

                                            } catch (Exception e) {
                                                if (e.getMessage() != null) {
                                                    checkAndDisplayErrorMessage(e.getMessage());
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                        break;

                                    case UPDATE_MOVIES:
                                        if (loggedIn) {

                                            movieResults = searchMovies();

                                            if (movieResults == null) {
                                                break;
                                            }

                                            displayMovieResults(movieResults);

                                            //choose movie to delete
                                            while (!valid) {
                                                System.out.println("Choose a movie from the list to update (list number): ");
                                                userInput = new Scanner(System.in);
                                                moviesChoice = userInput.nextInt();
                                                if (moviesChoice < 0 || moviesChoice > movieResults.length) {
                                                    valid = false;
                                                    System.out.println("\n please enter a valid choice.");
                                                } else {
                                                    MovieDTO toBeUpdated = movieResults[moviesChoice - 1];

                                                    // new details input
                                                    System.out.print("\nEnter new Genre: ");
                                                    userInput = new Scanner(System.in);
                                                    String newGenre = userInput.nextLine();
                                                    toBeUpdated.setGenre(newGenre);

                                                    System.out.print("\nEnter new Runtime: ");
                                                    userInput = new Scanner(System.in);
                                                    String newRuntime = userInput.nextLine();
                                                    toBeUpdated.setRuntime(newRuntime);

                                                    System.out.print("\nEnter new Year: ");
                                                    userInput = new Scanner(System.in);
                                                    String newYear = userInput.nextLine();
                                                    toBeUpdated.setYear(newYear);

                                                    request = ServiceDetails.UPDATE_MOVIES_COMMAND
                                                            + ServiceDetails.BREAKINGCHARACTERS
                                                            + toBeUpdated.toJSONString()
                                                            + ServiceDetails.BREAKINGCHARACTERS
                                                            + toBeUpdated.getId();

                                                    // send the request to the server
                                                    try {
                                                        // send through output stream
                                                        sendRequest();
                                                        // get response back from the server
                                                        receiveResponse();
                                                        // close the connection 
                                                        closeConnection();

                                                        // deal with response data
                                                        if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                                            System.out.println("An error has occurred, please try again!");
                                                            break;
                                                        } else {
                                                            System.out.println("The Movie " + toBeUpdated.getTitle() + "updated successfully!");
                                                            break;
                                                        }

                                                    } catch (Exception e) {
                                                        if (e.getMessage() != null) {
                                                            checkAndDisplayErrorMessage(e.getMessage());
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            }
                                            valid = false;
                                        }
                                        break;

                                    case BACK:
                                        moviesValid = true;

                                        break;

                                    default:
                                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                                        System.out.println("!!!!! Please enter a valid option !!!!!");
                                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                                        break;
                                }
                            }
                        }
                        break;

                    case EXIT:
                        System.out.println("\n Thank you for using our service. Good Bye.");
                        valid = true;
                        break;

                    default:
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        System.out.println("!!!!! Please enter a valid option !!!!!");
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        break;

                }
            } catch (Exception e) {
                if (e.getMessage() != null) {
                    checkAndDisplayErrorMessage(e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    public static MovieDTO[] searchMovies() {
        boolean valid = false;
        Scanner input;
        String inputString = "";
        int choice = 0;

        while (!valid) {
            try {
                System.out.println("----------------------------------------");
                System.out.println("What would you like to search the movies by?"
                        + "\n1. Search by title"
                        + "\n2. Search by genre"
                        + "\n3. Search by year"
                        + "\n4. Go Back");
                System.out.println("----------------------------------------");
                System.out.print("\nEnter option: ");
                input = new Scanner(System.in);
                choice = input.nextInt();

                if (choice > 4 || choice < 0) {
                    throw new Exception("No Such Option");
                }

                switch (choice) {
                    case BY_NAME:
                        while (!valid) {
                            System.out.println("----------------------------------------");
                            System.out.print("\nEnter the title to search: ");
                            input = new Scanner(System.in);
                            inputString = input.nextLine();

                            removeBreakingCharacters(inputString);

                            request = ServiceDetails.GET_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                    + ServiceDetails.NAME_COMMAND + ServiceDetails.BREAKINGCHARACTERS + inputString;

                            // check if request already exists in cache
                            if (cache.containsKey(request)) {
                                System.out.println("Found duplicate request. Retrieving from cache..");

                                JSONObject[] responseJSON = ServiceDetails.stringToJSON(cache.get(request));
                                MovieDTO[] results = MovieDTO.JSONToMovieDTO(responseJSON);
                                return results;
                            }

                            // send the request to the server
                            try {
                                // send through output stream
                                sendRequest();
                                // get response back from the server
                                receiveResponse();
                                // close the connection 
                                closeConnection();

                                // deal with response data
                                if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                    System.out.println("----------------------------------------");
                                    System.out.println("Sorry, we couldn't find any movies with that name.");
                                } else {
                                    // store request and response in cache
                                    cache.put(request, response);

                                    JSONObject[] responseJSON = ServiceDetails.stringToJSON(response);
                                    MovieDTO[] results = MovieDTO.JSONToMovieDTO(responseJSON);
                                    return results;
                                }

                            } catch (Exception e) {
                                checkAndDisplayErrorMessage(e.getMessage());
                                e.printStackTrace();
                            }
                        }

                        break;
                    case BY_GENRE:
                        while (!valid) {
                            System.out.println("----------------------------------------");
                            System.out.print("\nEnter the genre to search: ");
                            input = new Scanner(System.in);
                            inputString = input.nextLine();

                            removeBreakingCharacters(inputString);

                            request = ServiceDetails.GET_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                    + ServiceDetails.GENRE_COMMAND + ServiceDetails.BREAKINGCHARACTERS + inputString;

                            // check if request already exists in cache
                            if (cache.containsKey(request)) {
                                System.out.println("Found duplicate request. Retrieving from cache..");

                                JSONObject[] responseJSON = ServiceDetails.stringToJSON(cache.get(request));
                                MovieDTO[] results = MovieDTO.JSONToMovieDTO(responseJSON);
                                return results;
                            }

                            // send the request to the server
                            try {
                                // send through output stream
                                sendRequest();
                                // get response back from the server
                                receiveResponse();
                                // close the connection 
                                closeConnection();

                                // deal with response data
                                if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                    System.out.println("----------------------------------------");
                                    System.out.println("Sorry, we couldn't find any movies with that genre.");
                                } else {
                                    // store request and response in cache
                                    cache.put(request, response);

                                    JSONObject[] responseJSON = ServiceDetails.stringToJSON(response);
                                    MovieDTO[] results = MovieDTO.JSONToMovieDTO(responseJSON);
                                    return results;
                                }

                            } catch (Exception e) {
                                checkAndDisplayErrorMessage(e.getMessage());
                                e.printStackTrace();
                            }
                        }

                        break;
                    case BY_YEAR:
                        System.out.println("----------------------------------------");
                        System.out.print("\nEnter the year to search: ");
                        input = new Scanner(System.in);
                        inputString = input.nextLine();

                        removeBreakingCharacters(inputString);

                        request = ServiceDetails.GET_MOVIES_COMMAND + ServiceDetails.BREAKINGCHARACTERS
                                + ServiceDetails.YEAR_COMMAND + ServiceDetails.BREAKINGCHARACTERS + inputString;

                        // check if request already exists in cache
                        if (cache.containsKey(request)) {
                            System.out.println("Found duplicate request. Retrieving from cache..");

                            JSONObject[] responseJSON = ServiceDetails.stringToJSON(cache.get(request));
                            MovieDTO[] results = MovieDTO.JSONToMovieDTO(responseJSON);
                            return results;
                        }

                        // send the request to the server
                        try {
                            // send through output stream
                            sendRequest();
                            // get response back from the server
                            receiveResponse();
                            // close the connection 
                            closeConnection();

                            // deal with response data
                            if (response.equals(ServiceDetails.FAILED_RESPONSE)) {
                                System.out.println("----------------------------------------");
                                System.out.println("Sorry, we couldn't find any movies in that year.");
                            } else {
                                // store request and response in cache
                                cache.put(request, response);

                                JSONObject[] responseJSON = ServiceDetails.stringToJSON(response);
                                MovieDTO[] results = MovieDTO.JSONToMovieDTO(responseJSON);
                                return results;
                            }

                        } catch (Exception e) {
                            if (e.getMessage() != null) {
                                checkAndDisplayErrorMessage(e.getMessage());
                                e.printStackTrace();
                            }
                        }
                        break;

                    case EXIT:
                        valid = true;
                        break;
                }
            } catch (Exception e) {
                valid = false;
                checkAndDisplayErrorMessage(e.getMessage());
            }
        }
        valid = false;

        return null;
    }

    public static String removeBreakingCharacters(String input) {

        input = input.trim().replaceAll(ServiceDetails.BREAKINGCHARACTERS, "");

        return input;
    }

    public static void displayMovieResults(MovieDTO[] movieResults) {
        if (movieResults.length > 0) {
            for (int i = 0; i < movieResults.length; i++) {
                System.out.print("\n" + (i + 1) + ". " + movieResults[i].toString());
            }
        } else {
            System.out.println("No movies to display.");
        }
    }

    public static void sendRequest() throws Exception {

        connectionSocket = new Socket(ServiceDetails.SERVERLOCATION, ServiceDetails.SERVERPORT);
        connectionSocket.setSoTimeout(9000);

        outToServer = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));

        outToServer.println(request);
        outToServer.flush();
    }

    public static void receiveResponse() throws Exception {
        inFromServer = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
        response = inFromServer.readLine();

    }

    public static void closeConnection() throws Exception {
        connectionSocket.close();
        outToServer.close();
        inFromServer.close();
    }

    public static String printArray(Object[] array) {
        String returnString = "";

        for (int i = 0; i < array.length; i++) {
            returnString += "\n" + array[i].toString();

        }

        return returnString;

    }

    public static void checkAndDisplayErrorMessage(String message) {
        System.out.println("\nError =  " + message + "\n");
        if (message.equals("Read timed out")) {
            System.out.println("\nSorry, the server is currently busy. Please try again shortly.\n");
        } else if (message.equals("Connection refused: connect")) {
            System.out.println("\nSorry, the server is currently unavailable. Please try again shortly.\n");
        } else {
            System.out.println("\nSorry an unknown error occures. Please Try Again.\n");
        }
    }

    public static boolean isValidPassword(String password) {
        if (!password.matches(".{8,}")) {
            return false;
        } else if (!password.matches("(.*)?[0-9](.*)?")) {
            return false;
        } else if (!password.matches("(.*)?[A-Z](.*)?")) {
            return false;
        } else if (!password.matches("(.*)?[a-z](.*)?")) {
            return false;
        }

        return true;
    }
}
