package core;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Paudric
 */
public class MovieDTO {

    private int id;
    private int copies;
    private final String title;
    private String genre;
    private String director;
    private String runtime;
    private String plot;
    private String location;
    private String poster;
    private String rating;
    private String format;
    private String year;
    private String starring;
    private String barcode;
    private String user_rating;

    public MovieDTO(String title) {
        this.title = title;
    }

    public MovieDTO(String title, String genre, String director, String runtime, String plot, String location, String poster, String rating, String format, String year, String starring, int copies, String barcode, String user_rating) {
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.runtime = runtime;
        this.plot = plot;
        this.location = location;
        this.poster = poster;
        this.rating = rating;
        this.format = format;
        this.year = year;
        this.starring = starring;
        this.copies = copies;
        this.barcode = barcode;
        this.user_rating = user_rating;
    }

    public MovieDTO(int id, String title, String genre, String director, String runtime, String plot, String location, String poster, String rating, String format, String year, String starring, int copies, String barcode, String user_rating) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.runtime = runtime;
        this.plot = plot;
        this.location = location;
        this.poster = poster;
        this.rating = rating;
        this.format = format;
        this.year = year;
        this.starring = starring;
        this.copies = copies;
        this.barcode = barcode;
        this.user_rating = user_rating;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getDirector() {
        return director;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getPlot() {
        return plot;
    }

    public String getLocation() {
        return location;
    }

    public String getPoster() {
        return poster;
    }

    public String getRating() {
        return rating;
    }

    public String getFormat() {
        return format;
    }

    public String getYear() {
        return year;
    }

    public String getStarring() {
        return starring;
    }

    public int getCopies() {
        return copies;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getUser_rating() {
        return user_rating;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setStarring(String starring) {
        this.starring = starring;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setUser_rating(String user_rating) {
        this.user_rating = user_rating;
    }

    @Override
    public String toString() {
        String split = "";
        while(!plot.isEmpty()) {
            if (plot.length() >= 40) {
                split += plot.substring(0, 39) + "\n";
                plot = plot.substring(39);
            } else {
                split += plot + "\n";
                plot = "";
            }
        }
        
        return title + "\n"
                + "*****************************************" + "\n"
                + "Year: " + year + "\n"
                + "Genre: " + genre + "\n"
                + "Director: " + director + "\n"
                + "Starring: " + starring + "\n"
                + "Rating: " + rating + "\n"
                + "Runtime: " + runtime + "\n"
                + "Synopsis: \n\n"
                + split + "\n"
                + "*****************************************";
    }

    public String toJSONString() {
        this.plot = this.plot.replaceAll("\"", "");

        return "{" + "\"id\":" + "\"" + this.id + "\"," + "\"title\":" + "\"" + this.title + "\","
                + "\"genre\":" + "\"" + this.genre + "\"," + "\"director\":" + "\"" + this.director + "\","
                + "\"runtime\":" + "\"" + this.runtime + "\"," + "\"plot\":" + "\"" + this.plot + "\","
                + "\"location\":" + "\"" + this.location + "\"," + "\"poster\":" + "\"" + this.poster + "\","
                + "\"rating\":" + "\"" + this.rating + "\"," + "\"format\":" + "\"" + this.format + "\","
                + "\"year\":" + "\"" + this.year + "\"," + "\"starring\":" + "\"" + this.starring + "\","
                + "\"copies\":" + "\"" + this.copies + "\"," + "\"barcode\":" + "\"" + this.barcode + "\","
                + "\"user_rating\":" + "\"" + this.user_rating + "\"}";
    }

    public static MovieDTO[] JSONToMovieDTO(JSONObject[] responseJSON) throws ParseException {
        MovieDTO[] result = new MovieDTO[responseJSON.length];

        for (int i = 0; i < result.length; i++) {
            int id = Integer.parseInt((String) responseJSON[i].getOrDefault("id", "0"));
            String title = (String) responseJSON[i].get("title");
            String genre = (String) responseJSON[i].get("genre");
            String director = (String) responseJSON[i].get("director");
            String runtime = (String) responseJSON[i].get("runtime");
            String plot = (String) responseJSON[i].get("plot");
            String location = (String) responseJSON[i].get("location");
            String poster = (String) responseJSON[i].get("poster");
            String rating = (String) responseJSON[i].get("rating");
            String format = (String) responseJSON[i].get("format");
            String year = (String) responseJSON[i].get("year");
            String starring = (String) responseJSON[i].get("starring");
            int copies = Integer.parseInt((String) responseJSON[i].get("copies"));
            String barcode = (String) responseJSON[i].get("barcode");
            String user_rating = (String) responseJSON[i].get("user_rating");

            result[i] = new MovieDTO(
                    id,
                    title,
                    genre,
                    director,
                    runtime,
                    plot,
                    location,
                    poster,
                    rating,
                    format,
                    year,
                    starring,
                    copies,
                    barcode,
                    user_rating
            );
        }

        return result;
    }
}
