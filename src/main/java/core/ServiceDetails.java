/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 *
 * @author Nick D
 */
public class ServiceDetails {

    // connections stuff
    public static final int SERVERPORT = 60000;
    public static final String SERVERLOCATION = "localhost";
    
    // protocols stuff
    public static final String BREAKINGCHARACTERS = "%%";
    public static final String GET_MOVIES_COMMAND = "GETM";
    public static final String LOGIN_COMMAND = "LOGN";
    public static final String REGISTER_COMMAND = "REGS";
    public static final String WATCH_MOVIES_COMMAND = "WTCH";
    public static final String ADD_MOVIES_COMMAND = "ADDM";
    public static final String DELETE_MOVIES_COMMAND = "DELT";
    public static final String UPDATE_MOVIES_COMMAND = "UPDM";
    public static final String ID_COMMAND = "ID";
    public static final String GENRE_COMMAND = "GNRE";
    public static final String YEAR_COMMAND = "YEAR";
    public static final String NAME_COMMAND = "NAME";
    public static final String RECOMMEND_COMMAND = "RECM";
    public static final String USER_NOT_FOUND_RESPONSE = "NOTF";
    public static final String FAILED_RESPONSE = "FAIL";
    public static final String SUCCESSFUL_RESPONSE = "SUCS";

    // method that parses JSON format string to and array of JSONObjects
    public static JSONObject[] stringToJSON(String JSONString) throws ParseException {
        String[] objects = JSONString.split(core.ServiceDetails.BREAKINGCHARACTERS);
        JSONObject[] result = new JSONObject[objects.length];
        
        
        JSONParser parser = new JSONParser();
        
        for (int i = 0; i < result.length; i++) {
            result[i] = (JSONObject) parser.parse(objects[i]);
        }

        return result;
    }
}
