/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 *
 * @author Nick D
 */
public class WatchHistoryDTO
{

    
    private int watchHistoryId = 0;
    private int user_id;
    private int movie_id;

    public WatchHistoryDTO(int watchHistoryId, int user_id, int movie_id)
    {
        this.watchHistoryId = watchHistoryId;
        this.user_id = user_id;
        this.movie_id = movie_id;
    }

    public WatchHistoryDTO(int user_id, int movie_id)
    {
        this.user_id = user_id;
        this.movie_id = movie_id;
    }

    public int getWatchHistoryId()
    {
        return watchHistoryId;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public int getMovie_id()
    {
        return movie_id;
    }

    @Override
    public String toString()
    {
        return "WatchHistoryDTO{" + "watchHistoryId=" + watchHistoryId + ", user_id=" + user_id
                + ", movie_id=" + movie_id + '}';
    }

    public String toJSONString()
    {
        return "{" + "\"WatchHistoryId\":" + "\"" + this.watchHistoryId + "\"," + "\"UserId\":" + "\""
                + this.user_id + "\"," + "\"MovieId\":" + "\"" + this.movie_id + "\"}";
    }
}
