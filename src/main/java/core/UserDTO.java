/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 *
 * @author Nick D
 */
public class UserDTO
{

    
    private String username;
    private String password;
    private int user_id = 0;
    private int age;

    // write to JSON
    public UserDTO(String username, String password, int age)
    {
        this.username = username;
        this.password = password;
        this.age = age;
    }

    public UserDTO(int user_id, String username, String password, int age)
    {
        this.username = username;
        this.password = password;
        this.age = age;
        this.user_id = user_id;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public int getAge()
    {
        return age;
    }

    @Override
    public String toString()
    {
        return "UserDTO{" + "username=" + username + ", password=" + password + ", user_id=" + user_id + ", age=" + age + '}';
    }

    public String toJSONString()
    {
        return "{" + "\"Username\":" + "\"" + this.username + "\"," + "\"Password\":" + "\""
                + this.password + "\"," + "\"UserId\":" + "\"" + this.user_id + "\"Age\":" + "\"" + this.age + "\"}";
    }

}
