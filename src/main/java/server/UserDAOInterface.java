/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import core.UserDTO;
import core.MovieDTO;

/**
 *
 * @author Nick D
 */
public interface UserDAOInterface
{
    

    // methods for querying the customers table
    public UserDTO getUser(String username, String password) throws DAOException;

    public boolean registerUser(UserDTO user) throws DAOException;

    public boolean deleteUser(int user_id) throws DAOException;

    public boolean updateUser(int id, String newUsername, String newPassword, int newAge) throws DAOException;

    public String watchMovie(int user_id, MovieDTO movie) throws DAOException;
}
