/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import core.MovieDTO;
import core.UserDTO;
import core.WatchHistoryDTO;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import core.ServiceDetails;
import java.util.Arrays;
import org.json.simple.JSONObject;

/**
 *
 * @author Nick D and Paudric S
 */
public class ServerThread extends Thread {

    private Socket connectionSocket = null;

    private BufferedReader inFromClient = null;
    private PrintWriter outToClient = null;

    private String request[];
    private String command = "";
    private MovieDTO[] movieResults;
    private String response = "";

    private MysqlMovieDAO movieDao = new MysqlMovieDAO();
    private MysqlWatchHistoryDAO historyDao = new MysqlWatchHistoryDAO();
    private MysqlUserDAO userDao = new MysqlUserDAO();

    public ServerThread(Socket connectionSocket) {
        this.connectionSocket = connectionSocket;
    }

    @Override
    public void run() {
        try {

            Server.threadCount++;

            inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));

            // break up the request into components by the breaking characters from the servicedetails
            request = inFromClient.readLine().split(core.ServiceDetails.BREAKINGCHARACTERS);
            command = request[0];

            // identify the command
            switch (command) {
                case ServiceDetails.LOGIN_COMMAND:
                    //LOGN%%username%%password

                    // try to retrieve user
                    UserDTO userToBeLoggedIn = userDao.getUser(request[1], request[2]);

                    // check if result is not null
                    if (userToBeLoggedIn != null) {
                        // send back just user id and username and also age, dont send password
                        // user_id%%username%%age
                        response = userToBeLoggedIn.getUser_id() + ServiceDetails.BREAKINGCHARACTERS + userToBeLoggedIn.getUsername() + ServiceDetails.BREAKINGCHARACTERS + userToBeLoggedIn.getAge();
                    } else {
                        //assign failed message
                        response = ServiceDetails.USER_NOT_FOUND_RESPONSE;
                    }

                    // send the response string
                    outToClient = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
                    outToClient.println(response);
                    outToClient.flush();

                    break;

                case ServiceDetails.REGISTER_COMMAND:
                    //REGS%%username%%password%%age

                    // make user dto to be added to the database
                    UserDTO userToBeRegistered = new UserDTO(request[1], request[2], Integer.parseInt(request[3]));

                    // register the user and check if failed or successful
                    if (userDao.registerUser(userToBeRegistered)) {
                        // if successful then send success response
                        response = ServiceDetails.SUCCESSFUL_RESPONSE;
                    } else {
                        // if failed then send failed response
                        response = ServiceDetails.FAILED_RESPONSE;
                    }

                    // send the response string
                    outToClient = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
                    outToClient.println(response);
                    outToClient.flush();

                    break;

                case core.ServiceDetails.GET_MOVIES_COMMAND:

                    // query the database
                    // check if there is a second command
                    if (request.length > 1) {
                        String secondCommand = request[1];

                        switch (secondCommand) {
                            case ServiceDetails.ID_COMMAND:
                                movieResults = movieDao.findMoviesById(Integer.parseInt(request[2]));
                                break;

                            case ServiceDetails.NAME_COMMAND:
                                movieResults = movieDao.findMoviesByName(request[2]);
                                break;

                            case ServiceDetails.GENRE_COMMAND:
                                movieResults = movieDao.findMoviesByGenre(request[2], 10);
                                break;

                            case ServiceDetails.YEAR_COMMAND:
                                movieResults = movieDao.findMoviesByYear(request[2]);
                                break;
                        }

                    } else {
                        movieResults = movieDao.getAllMovies();
                    }

                    if (movieResults == null) {
                        response = ServiceDetails.FAILED_RESPONSE;
                    } else {
                        // convert array to big long string in JSON format separating each of the results with the breaking characters
                        response = toJSONString(movieResults);
                    }

                    // send the response string
                    outToClient = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
                    outToClient.println(response);
                    outToClient.flush();

                    break;

                case ServiceDetails.WATCH_MOVIES_COMMAND:
                    boolean isSuccessful = historyDao.addWatchHistory(new WatchHistoryDTO(Integer.parseInt(request[1]), Integer.parseInt(request[2])));

                    if (!isSuccessful) {
                        response = ServiceDetails.FAILED_RESPONSE;
                    } else {
                        response = ServiceDetails.SUCCESSFUL_RESPONSE;
                    }

                    // send the response string
                    outToClient = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
                    outToClient.println(response);
                    outToClient.flush();

                    break;

                case ServiceDetails.RECOMMEND_COMMAND:
                    movieResults = movieDao.recommendMovies(historyDao.getWatchedGenres(Integer.parseInt(request[1])), 5);

                    // check if result is less than 5
                    if (movieResults.length < 5)
                    {
                        // then just send the first 5 movies from database
                        movieResults = movieDao.findMoviesByGenre("action", 5);
                    }

                    // convert array to big long string in JSON format separating each of the results with the breaking characters
                    response = toJSONString(movieResults);

                    // send the response string
                    outToClient = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
                    outToClient.println(response);
                    outToClient.flush();

                    break;

                case core.ServiceDetails.ADD_MOVIES_COMMAND:
                    // pull down the movie to be added in json format
                    String movieJSON = request[1];

                    // convert to real JSONObject
                    JSONObject[] movieJSONarray = ServiceDetails.stringToJSON(movieJSON);

                    // convert to MovieDTO
                    MovieDTO movieToBeAdded = MovieDTO.JSONToMovieDTO(movieJSONarray)[0];

                    //add to database
                    if (movieDao.addMovie(movieToBeAdded)) {
                        response = ServiceDetails.SUCCESSFUL_RESPONSE;
                    } else {
                        response = ServiceDetails.FAILED_RESPONSE;
                    }

                    // send response message
                    outToClient = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
                    outToClient.println(response);
                    outToClient.flush();

                    break;

                case core.ServiceDetails.DELETE_MOVIES_COMMAND:
                    // delete movie based on the passed user_id
                    if (movieDao.deleteMovie(Integer.parseInt(request[1]))) {
                        response = ServiceDetails.SUCCESSFUL_RESPONSE;
                    } else {
                        response = ServiceDetails.FAILED_RESPONSE;
                    }

                    // send response message
                    outToClient = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
                    outToClient.println(response);
                    outToClient.flush();

                    break;

                case core.ServiceDetails.UPDATE_MOVIES_COMMAND:
                    //UPDM%%{ movie in json format... }%%movie_id
                    if (movieDao.updateMovie(MovieDTO.JSONToMovieDTO(ServiceDetails.stringToJSON(request[1]))[0], Integer.parseInt(request[2]))) {
                        response = ServiceDetails.SUCCESSFUL_RESPONSE;
                    } else {
                        response = ServiceDetails.FAILED_RESPONSE;
                    }

                    // send response message
                    outToClient = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
                    outToClient.println(response);
                    outToClient.flush();

                    break;

                default:
                    break;

            }

            Server.threadCount--;

        } catch (Exception e) {
            System.out.println("\nError = " + e.getMessage() + "\n");
            e.printStackTrace();
        }
    }

    public static String toJSONString(MovieDTO[] movies) {
        String result = "";

        for (int i = 0; i < movies.length; i++) {
            result += movies[i].toJSONString() + core.ServiceDetails.BREAKINGCHARACTERS;
        }

        return result;
    }

    public static String toJSONString(WatchHistoryDTO[] watchHistory) {
        String result = "";

        for (int i = 0; i < watchHistory.length; i++) {
            result += watchHistory[i].toJSONString() + core.ServiceDetails.BREAKINGCHARACTERS;
        }

        return result;
    }

    public static String toJSONString(UserDTO[] user) {
        String result = "";

        for (int i = 0; i < user.length; i++) {
            result += user[i].toJSONString() + core.ServiceDetails.BREAKINGCHARACTERS;
        }

        return result;
    }
}
