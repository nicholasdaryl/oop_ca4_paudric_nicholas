/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import core.WatchHistoryDTO;
import core.MovieDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Nick D
 */
public class MysqlWatchHistoryDAO extends MysqlDAO implements WatchHistoryDAOInterface
{

    
    Connection conn = null;
    PreparedStatement stmt = null;

    public WatchHistoryDTO[] getAllWatchHistoryByUserId(int user_id) throws DAOException
    {
        try
        {
            conn = getConnection();

            stmt = conn.prepareStatement("SELECT * FROM watch_histories WHERE user_id = ?");
            stmt.setInt(1, user_id);
            ResultSet rs = stmt.executeQuery();
            if (rs.last())
            {
                WatchHistoryDTO[] results = new WatchHistoryDTO[rs.getRow()];
                rs.beforeFirst();

                while (rs.next())
                {
                    results[rs.getRow() - 1] = new WatchHistoryDTO(rs.getInt("history_id"), rs.getInt("user_id"), rs.getInt("movie_id"));
                }
                return results;
            } else
            {
                return null;
            }

        } catch (SQLException ex)
        {
            return null;
        } finally
        {
            closeConnection(conn);
        }
    }

    @Override
    public boolean addWatchHistory(WatchHistoryDTO watchHistory) throws DAOException
    {
        conn = getConnection();
        try
        {
            stmt = conn.prepareStatement("INSERT INTO watch_histories (user_id, movie_id) VALUES (?,?)");
            stmt.setInt(1, watchHistory.getUser_id());
            stmt.setInt(2, watchHistory.getMovie_id());
            stmt.executeUpdate();

            return true;
        } catch (SQLException e)
        {
            return false;
        } finally
        {
            closeConnection(conn);
        }
    }

    public Map<String, Integer> getWatchedGenres(int user_id) throws DAOException
    {
        // get all the history of the user first
        MysqlWatchHistoryDAO historyDao = new MysqlWatchHistoryDAO();
        MysqlMovieDAO movieDao = new MysqlMovieDAO();
        WatchHistoryDTO[] history = historyDao.getAllWatchHistoryByUserId(user_id);

        Map<String, Integer> genreMap = new HashMap<>();

        if (history != null)
        {
            for (int i = 0; i < history.length; i++)
            {
                MovieDTO movie = movieDao.findMoviesById(history[i].getMovie_id())[0];
                String[] genres = movie.getGenre().split(",");
                for (int j = 0; j < genres.length; j++)
                {
                    genres[j] = genres[j].trim();
                    if (genreMap.size() == 0)
                    {
                        genreMap.put(genres[j], 0);
                    }

                    if (genreMap.containsKey(genres[j]))
                    {
                        genreMap.replace(genres[j], genreMap.get(genres[j]) + 1);
                    } else
                    {
                        genreMap.put(genres[j], 1);
                    }
                }
            }
        }

        return genreMap;
    }

}
