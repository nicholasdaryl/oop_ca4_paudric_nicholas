/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import core.WatchHistoryDTO;
import core.UserDTO;
import core.MovieDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Nick D
 */
public class MysqlUserDAO extends MysqlDAO implements UserDAOInterface
{

    
    Connection conn = null;
    PreparedStatement stmt = null;

    public boolean registerUser(UserDTO user) throws DAOException
    {
        try
        {
            conn = getConnection();

            stmt = conn.prepareStatement("INSERT INTO users (username, password, age) VALUES (?,?,?)");
            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getPassword());
            stmt.setInt(3, user.getAge());
            stmt.executeUpdate();

        } catch (Exception ex)
        {

            return false;
        } finally
        {
            closeConnection(conn);
        }
        return true;
    }

    public boolean deleteUser(int user_id) throws DAOException
    {
        try
        {
            conn = getConnection();

            stmt = conn.prepareStatement("DELETE FROM users WHERE user_id = ?");
            stmt.setInt(1, user_id);
            stmt.executeUpdate();

        } catch (Exception ex)
        {

            return false;
        } finally
        {
            closeConnection(conn);
        }
        return true;
    }

    public UserDTO getUser(String username, String password) throws DAOException
    {
        conn = getConnection();
        // query here and return set of movies
        try
        {
            stmt = conn.prepareStatement("SELECT * FROM users WHERE username = ? AND password = ?");
            stmt.setString(1, username);
            stmt.setString(2, password);
            ResultSet rs = stmt.executeQuery();
            if (rs.last())
            {
                return new UserDTO(rs.getInt("user_id"), rs.getString("username"), rs.getString("password"), rs.getInt("age"));

            } else
            {
                return null;
            }
        } catch (SQLException ex)
        {
            return null;
        } finally
        {
            closeConnection(conn);
        }
    }

    public boolean updateUser(int id, String newUsername, String newPassword, int newAge) throws DAOException
    {
        try
        {
            conn = getConnection();

            stmt = conn.prepareStatement("UPDATE users SET username = ?, password = ?, age = ? WHERE user_id = ?");
            stmt.setString(1, newUsername);
            stmt.setString(2, newPassword);
            stmt.setInt(3, newAge);
            stmt.setInt(4, id);
            stmt.executeUpdate();

        } catch (Exception ex)
        {

            return false;
        } finally
        {
            closeConnection(conn);
        }
        return true;
    }

    @Override
    public String watchMovie(int user_id, MovieDTO movie) throws DAOException
    {
        conn = getConnection();

        MysqlWatchHistoryDAO historyDao = new MysqlWatchHistoryDAO();

        historyDao.addWatchHistory(new WatchHistoryDTO(user_id, movie.getId()));

        return "You have watched " + movie.getTitle() + ".";
    }

}
