/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import core.WatchHistoryDTO;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Nick D
 */
public interface WatchHistoryDAOInterface
{

    
    public WatchHistoryDTO[] getAllWatchHistoryByUserId(int user_id) throws DAOException;

    public boolean addWatchHistory(WatchHistoryDTO watchHistory) throws DAOException;

    public Map<String, Integer> getWatchedGenres(int user_id) throws DAOException;
}
