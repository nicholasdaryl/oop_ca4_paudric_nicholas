package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlDAO
{

    public Connection getConnection() throws DAOException
            
    {
        //change this according to the database on your local machine
        String dbName = "moviesoop";

        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/" + dbName + "?serverTimezone=UTC";
        String username = "root";
        String password = "";
        Connection conn = null;

        
        try
        {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex)
        {
            System.out.println("Connection failed " + ex.getMessage());
            System.exit(1);
        }
        return conn;
    }

    public void closeConnection(Connection conn) throws DAOException
    {
        try
        {
            if (conn != null)
            {
                conn.close();
                conn = null;
            }
        } catch (SQLException e)
        {
            System.out.println("Failed to free the connection: " + e.getMessage());
            System.exit(1);
        }
    }
}
