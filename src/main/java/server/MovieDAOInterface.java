/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

/**
 *
 * @author Paudric
 */
import core.MovieDTO;
import java.util.List;
import java.util.Map;

public interface MovieDAOInterface
{
    
    

    public MovieDTO[] getAllMovies() throws DAOException;

    public boolean deleteMovie(int movie_id) throws DAOException;

    public boolean addMovie(MovieDTO movie) throws DAOException;

    public boolean updateMovie(MovieDTO movie, int movieId) throws DAOException;
    
    public MovieDTO[] findMoviesByName(String movieName) throws DAOException;

    public MovieDTO[] findMoviesByYear(String movieYear) throws DAOException;

    public MovieDTO[] findMoviesByGenre(String genre, int limit) throws DAOException;

    public MovieDTO[] findMoviesById(int movieId) throws DAOException;

    public MovieDTO[] recommendMovies(Map<String, Integer> genreMap, int limit) throws DAOException;
}
