package server;


import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Paudric
 */

public class Server {

    public static int threadCount = 0;
    private static int maxConcurrentThreads = 5;

    public static void main(String[] args) {
        try {

            
            boolean continueRunning = true;

            ServerSocket listeningSocket = new ServerSocket(core.ServiceDetails.SERVERPORT);
            
            System.out.println("Server Location: " + core.ServiceDetails.SERVERLOCATION + ", Listening on port: " + core.ServiceDetails.SERVERPORT);

            while (continueRunning) {
                if (threadCount < maxConcurrentThreads-1) {
                    
                    ServerThread t = new ServerThread(listeningSocket.accept());
                    t.start();
                    
                } else {
                    System.out.println("Thread count: " + threadCount);
                    System.out.println("No threads available");
                }
            }
        } catch (Exception e) {
            System.out.println("\nError = " + e.getMessage() + "\n");
            e.printStackTrace();
        }
    }
}
