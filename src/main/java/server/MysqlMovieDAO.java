package server;

/**
 *
 * @author Paudric
 */
import core.MovieDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

public final class MysqlMovieDAO extends MysqlDAO implements MovieDAOInterface {

    Connection conn = null;
    PreparedStatement stmt = null;

    //Get all movies
    public MovieDTO[] getAllMovies() throws DAOException {

        conn = getConnection();
        // query here and return set of movies
        try {
            stmt = conn.prepareStatement("SELECT * FROM movies LIMIT 20");
            ResultSet rs = stmt.executeQuery();

            if (rs.last()) {
                MovieDTO[] results = new MovieDTO[rs.getRow()];
                rs.beforeFirst();

                while (rs.next()) {
                    results[rs.getRow() - 1] = new MovieDTO(rs.getInt("id"), rs.getString("title"),
                            rs.getString("genre"), rs.getString("director"), rs.getString("runtime"),
                            rs.getString("plot"), rs.getString("location"), rs.getString("poster"),
                            rs.getString("rating"), rs.getString("format"), rs.getString("year"),
                            rs.getString("starring"), rs.getInt("copies"), rs.getString("barcode"),
                            rs.getString("user_rating"));
                }
                return results;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            return null;
        } finally {
            closeConnection(conn);
        }
    }

    //Find movie by name
    public MovieDTO[] findMoviesByName(String movieName) throws DAOException {
        conn = getConnection();
        // query here and return set of movies
        try {
            stmt = conn.prepareStatement("SELECT * FROM movies WHERE title LIKE ?");
            stmt.setString(1, "%" + movieName + "%");
            ResultSet rs = stmt.executeQuery();
            if (rs.last()) {
                MovieDTO[] results = new MovieDTO[rs.getRow()];
                rs.beforeFirst();

                while (rs.next()) {
                    results[rs.getRow() - 1] = new MovieDTO(rs.getInt("id"), rs.getString("title"),
                            rs.getString("genre"), rs.getString("director"), rs.getString("runtime"),
                            rs.getString("plot"), rs.getString("location"), rs.getString("poster"),
                            rs.getString("rating"), rs.getString("format"), rs.getString("year"),
                            rs.getString("starring"), rs.getInt("copies"), rs.getString("barcode"),
                            rs.getString("user_rating"));
                }
                return results;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            return null;
        } finally {
            closeConnection(conn);
        }
    }

    //Find movie by year
    public MovieDTO[] findMoviesByYear(String movieYear) throws DAOException {
        conn = getConnection();
        // query here and return set of movies
        try {
            stmt = conn.prepareStatement("SELECT * FROM movies WHERE year = ?");
            stmt.setString(1, movieYear);
            ResultSet rs = stmt.executeQuery();
            if (rs.last()) {
                MovieDTO[] results = new MovieDTO[rs.getRow()];
                rs.beforeFirst();

                while (rs.next()) {
                    results[rs.getRow() - 1] = new MovieDTO(rs.getInt("id"), rs.getString("title"),
                            rs.getString("genre"), rs.getString("director"), rs.getString("runtime"),
                            rs.getString("plot"), rs.getString("location"), rs.getString("poster"),
                            rs.getString("rating"), rs.getString("format"), rs.getString("year"),
                            rs.getString("starring"), rs.getInt("copies"), rs.getString("barcode"),
                            rs.getString("user_rating"));
                }
                return results;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            return null;
        } finally {
            closeConnection(conn);
        }
    }

    //Find movie by Id
    public MovieDTO[] findMoviesById(int movieId) throws DAOException {
        conn = getConnection();
        // query here and return set of movies
        try {
            stmt = conn.prepareStatement("SELECT * FROM movies WHERE id = ?");
            stmt.setInt(1, movieId);
            ResultSet rs = stmt.executeQuery();
            if (rs.last()) {
                MovieDTO[] results = new MovieDTO[rs.getRow()];
                rs.beforeFirst();

                while (rs.next()) {
                    results[rs.getRow() - 1] = new MovieDTO(rs.getInt("id"), rs.getString("title"),
                            rs.getString("genre"), rs.getString("director"), rs.getString("runtime"),
                            rs.getString("plot"), rs.getString("location"), rs.getString("poster"),
                            rs.getString("rating"), rs.getString("format"), rs.getString("year"),
                            rs.getString("starring"), rs.getInt("copies"), rs.getString("barcode"),
                            rs.getString("user_rating"));
                }
                return results;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            return null;
        } finally {
            closeConnection(conn);
        }
    }

    public MovieDTO[] findMoviesByGenre(String genre, int limit) throws DAOException {
        conn = getConnection();
        // query here and return set of movies
        try {
            stmt = conn.prepareStatement("SELECT * FROM movies WHERE genre LIKE ? LIMIT ?");
            stmt.setString(1, "%" + genre + "%");
            stmt.setInt(2, limit);
            ResultSet rs = stmt.executeQuery();
            if (rs.last()) {
                MovieDTO[] results = new MovieDTO[rs.getRow()];
                rs.beforeFirst();

                while (rs.next()) {
                    results[rs.getRow() - 1] = new MovieDTO(rs.getInt("id"), rs.getString("title"),
                            rs.getString("genre"), rs.getString("director"), rs.getString("runtime"),
                            rs.getString("plot"), rs.getString("location"), rs.getString("poster"),
                            rs.getString("rating"), rs.getString("format"), rs.getString("year"),
                            rs.getString("starring"), rs.getInt("copies"), rs.getString("barcode"),
                            rs.getString("user_rating"));
                }
                return results;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            return null;
        } finally {
            closeConnection(conn);
        }
    }

    //Add movie
    public boolean addMovie(MovieDTO movie) throws DAOException {
        conn = getConnection();
        // query here add movie
        try {
            stmt = conn.prepareStatement("INSERT INTO movies (title, genre, director, runtime, plot, "
                    + "location, poster, rating, format, year, starring, copies, barcode, user_rating)"
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            stmt.setString(1, movie.getTitle());
            stmt.setString(2, movie.getGenre());
            stmt.setString(3, movie.getDirector());
            stmt.setString(4, movie.getRuntime());
            stmt.setString(5, movie.getPlot());
            stmt.setString(6, movie.getLocation());
            stmt.setString(7, movie.getPoster());
            stmt.setString(8, movie.getRating());
            stmt.setString(9, movie.getFormat());
            stmt.setString(10, movie.getYear());
            stmt.setString(11, movie.getStarring());
            stmt.setInt(12, movie.getCopies());
            stmt.setString(13, movie.getBarcode());
            stmt.setString(14, movie.getUser_rating());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            return false;
        } finally {
            closeConnection(conn);
        }
        return true;

    }

    //Update movie
    public boolean updateMovie(MovieDTO movie, int movieId) throws DAOException {
        conn = getConnection();
        // query here update movie
        try {
            stmt = conn.prepareStatement("UPDATE movies SET title = ?, genre = ?, director = ?, runtime = ?, plot = ?, "
                    + "location = ?, poster = ?, rating = ?, format = ?, year = ?, starring = ?, copies = ?, barcode = ?,"
                    + "user_rating = ? "
                    + "WHERE id = ?");
            stmt.setString(1, movie.getTitle());
            stmt.setString(2, movie.getGenre());
            stmt.setString(3, movie.getDirector());
            stmt.setString(4, movie.getRuntime());
            stmt.setString(5, movie.getPlot());
            stmt.setString(6, movie.getLocation());
            stmt.setString(7, movie.getPoster());
            stmt.setString(8, movie.getRating());
            stmt.setString(9, movie.getFormat());
            stmt.setString(10, movie.getYear());
            stmt.setString(11, movie.getStarring());
            stmt.setInt(12, movie.getCopies());
            stmt.setString(13, movie.getBarcode());
            stmt.setString(14, movie.getUser_rating());
            stmt.setInt(15, movieId);
            stmt.executeUpdate();

        } catch (SQLException ex) {
            return false;
        } finally {
            closeConnection(conn);
        }
        return true;

    }

    public boolean deleteMovie(int movie_id) throws DAOException {
        conn = getConnection();
        // query here delete movie
        try {
            stmt = conn.prepareStatement("DELETE FROM movies WHERE id = ?");
            stmt.setInt(1, movie_id);
            stmt.executeUpdate();
            System.out.println("movie deleted");
        } catch (SQLException ex) {
            return false;
        } finally {
            closeConnection(conn);
        }
        return true;

    }
    

    @Override
    public MovieDTO[] recommendMovies(Map<String, Integer> genreMap, int limit) throws DAOException {
        String topGenre = "Action";
        int topCount = 0;
        MysqlMovieDAO movieDao = new MysqlMovieDAO();

        Iterator iterator = genreMap.keySet().iterator();

        // topCount = genreMap.get(iterator.next());
        for (String key : genreMap.keySet()) {
            if (genreMap.get(key) > topCount) {
                topCount = genreMap.get(key);
                topGenre = key;
            }
        }
        
        MovieDTO[] movieResults = movieDao.findMoviesByGenre(topGenre, limit);
        
        if (movieResults == null || movieResults.length < 5) {
            movieResults = movieDao.findMoviesByGenre("Action", limit);
        }
        
        return movieResults;
    }

}
