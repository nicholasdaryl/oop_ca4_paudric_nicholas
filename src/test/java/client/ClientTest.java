/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import core.MovieDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nick D
 */
public class ClientTest {
    
    @org.junit.Test
    public void testRemoveBreakingCharacters() {
        System.out.println("removeBreakingCharacters");
        String input = core.ServiceDetails.BREAKINGCHARACTERS + "foobar" + core.ServiceDetails.BREAKINGCHARACTERS;
        String expResult = "foobar";
        System.out.println("before = " + input);
        String result = Client.removeBreakingCharacters(input);
        System.out.println("after = " + result);
        assertEquals(expResult, result);
    }
}
