/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import core.MovieDTO;
import core.UserDTO;
import core.WatchHistoryDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nick D
 */
public class ServerThreadTest {

    /**
     * Test of toJSONString method, of class ServerThread.
     */
    @Test
    public void testToJSONString_MovieDTOArr() {
        System.out.println("toJSONString");
        MovieDTO[] movies = {new MovieDTO("title", "genre", "director", "runtime", "plot", "location", "poster", "rating", "format", "year", "starring", 2, "barcode", "user_rating")};
        String expResult = "{\"id\":\"0\",\"title\":\"title\",\"genre\":\"genre\",\"director\":\"director\",\"runtime\":\"runtime\",\"plot\":\"plot\",\"location\":\"location\",\"poster\":\"poster\",\"rating\":\"rating\",\"format\":\"format\",\"year\":\"year\",\"starring\":\"starring\",\"copies\":\"2\",\"barcode\":\"barcode\",\"user_rating\":\"user_rating\"}%%";
        String result = ServerThread.toJSONString(movies);
        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of toJSONString method, of class ServerThread.
     */
    @Test
    public void testToJSONString_WatchHistoryDTOArr() {
        System.out.println("toJSONString");
        WatchHistoryDTO[] watchHistory = {new WatchHistoryDTO(0, 0)};
        String expResult = "{\"WatchHistoryId\":\"0\",\"UserId\":\"0\",\"MovieId\":\"0\"}%%";
        String result = ServerThread.toJSONString(watchHistory);
        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of toJSONString method, of class ServerThread.
     */
    @Test
    public void testToJSONString_UserDTOArr() {
        System.out.println("toJSONString");
        UserDTO[] user = {new UserDTO("John", "password", 0)};
        String expResult = "{\"Username\":\"John\",\"Password\":\"password\",\"UserId\":\"0\"Age\":\"0\"}%%";
        String result = ServerThread.toJSONString(user);
        System.out.println(result);
        assertEquals(expResult, result);
    }
    
}
