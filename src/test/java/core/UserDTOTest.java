/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nick D
 */
public class UserDTOTest {
    /**
     * Test of toJSONString method, of class UserDTO.
     */
    @Test
    public void testToJSONString() {
        System.out.println("toJSONString");
        UserDTO instance = new UserDTO("John", "password", 0);
        String expResult = "{\"Username\":\"John\",\"Password\":\"password\",\"UserId\":\"0\"Age\":\"0\"}";
        String result = instance.toJSONString();
        assertEquals(expResult, result);
    }
    
}
