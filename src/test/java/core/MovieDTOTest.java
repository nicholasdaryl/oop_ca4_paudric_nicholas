/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nick D
 */
public class MovieDTOTest {
    /**
     * Test of toJSONString method, of class MovieDTO.
     */
    @Test
    public void testToJSONString() {
        System.out.println("toJSONString");
        MovieDTO instance = new MovieDTO("title", "genre", "director", "runtime", "plot", "location", "poster", "rating", "format", "year", "starring", 2, "barcode", "user_rating");
        String expResult = "{\"id\":\"0\",\"title\":\"title\",\"genre\":\"genre\",\"director\":\"director\",\"runtime\":\"runtime\",\"plot\":\"plot\",\"location\":\"location\",\"poster\":\"poster\",\"rating\":\"rating\",\"format\":\"format\",\"year\":\"year\",\"starring\":\"starring\",\"copies\":\"2\",\"barcode\":\"barcode\",\"user_rating\":\"user_rating\"}";
        String result = instance.toJSONString();
        assertEquals(expResult, result);
    }   
}
